from django.shortcuts import render
from django.conf import settings
from django.core.mail import send_mail


# Create your views here.

def index(request):
    context = {
        'data': {
            'dealers_in_cards': [{
                'image_path': 'webpage/cpvc.jpg',
                'main_title': 'CPVC Pipes & Fittings',
                'sub_title': '(ASTRAL,PRINCE,SUPREME)',
            }, {
                'image_path': 'webpage/upvc.jpeg',
                'main_title': 'UPVC Pipes & Fittings',
                'sub_title': '(ASTRAL,PRINCE,SUPREME)',
            }, {
                'image_path': 'webpage/swr.png',
                'main_title': 'SWR Pipes & Fittings',
                'sub_title': '(ASTRAL,PRINCE,SUPREME)',
            }, {
                'image_path': 'webpage/hdpe.jpg',
                'main_title': 'HDPE Pipes & Fittings',
            }, {
                'image_path': 'webpage/waterpump.jpeg',
                'main_title': 'Water Pumps',
            }, {
                'image_path': 'webpage/Sanitaryware.jpg',
                'main_title': 'Sanitary Wares',
            }, {
                'image_path': 'webpage/cpbathroom.jpg',
                'main_title': 'CP Bathroom Fittings & Accessories',
            }, {
                'image_path': 'webpage/zbracket.jpg',
                'main_title': 'Z brackets',
            }, {
                'image_path': 'webpage/ubolt.jpeg',
                'main_title': 'U Bolts & Support Brackets',
            }],
            'our_renowed_customers': [
                'Cello Plastics',
                'Narendra Plastics',
                'Narendra Polymers',
                'Flair',
                'Montex',
                'Bluplast',
                'And More',
            ]
        },

    }
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        phone = request.POST['phone_number']
        inq = request.POST['message']
        message = 'NAME:\t' + name + '\nEmail id:\t' + email + '\nPhone no:\t' + phone + '\ninquiry:\t' + inq

        send_mail('plumbstone site inquiry', message, settings.EMAIL_HOST_USER, ['plumbstone1962@gmail.com'],
                  fail_silently=False)

    return render(request, 'webpage/index.html', context)
